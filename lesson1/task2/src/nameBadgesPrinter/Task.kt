package nameBadgesPrinter

import java.util.*

// type your solution here

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    
    val name = scanner.next() + " " + scanner.next()
//    val name = readLine()!!
    println("*".repeat(name.length + 4))
    println("* $name *")
    println("*".repeat(name.length + 4))
}