package nameBadgesPrinter

import org.hyperskill.hstest.dev.stage.BaseStageTest
import org.hyperskill.hstest.dev.testcase.CheckResult
import org.hyperskill.test_utils.testCase
import kotlin.reflect.jvm.javaMethod


class Task1Test : BaseStageTest<Unit>(::main.javaMethod) {
    init {
        isTestingMain = true
    }
    
    override fun generateTestCases() = listOf(
            testCase(Unit, "")
    )
    
    override fun check(reply: String, clue: Unit): CheckResult {
        val badge = " _____________\n" +
                "| Hyper Skill |\n" +
                " ¯¯¯¯¯¯¯¯¯¯¯¯¯"
        
        if (reply.trim('\n', ' ') != badge.trim(' ')) {
            return CheckResult.FALSE("You printed the wrong badge! See example #1.")
        } else {
            return CheckResult.TRUE
        }
        
    }
}

